***Settings***
Library    SeleniumLibrary

***Variables***

***Test Cases***
คุณวรวิทย์ค้นหางาน Programmer และสมัครงานไม่สำเร็จขึ้นว่า Age out of range
    #0: ต้องทำอะไรบ้าง? 
    เข้าเว็บไปที่หน้าค้นหา 
    พิมพ์ค้นค้นหา "Programmer" และกดปุ่มค้นหา
    หน้าเว็บจะขึ้นอาชีพ Programmer 4 
    เลือกตำแหน่ง Junior Node Programmer 
    กดสมัคร 
    ระบบแสดงผลลัพธ์ Gender does not meet the conditions

***Keywords***
เข้าเว็บไปที่หน้าค้นหา
    Open Browser   http://localhost:3000/searchJob    Chrome
    Set Selenium Speed    0.5

พิมพ์ค้นค้นหา "Programmer" และกดปุ่มค้นหา
    Input text    id=search_text    Programmer
    Click Element    id=search_button

หน้าเว็บจะขึ้นอาชีพ Programmer 4
    Element Text Should Be    id=name_1    Python Programmer

เลือกตำแหน่ง Junior Node Programmer
    Click Element    id=show_detail_4
    Wait Until Element Contains    id=name    Junior Node Programmer


กดสมัคร 
    Click Element    id=apply_job
ระบบแสดงผลลัพธ์ Gender does not meet the conditions
    Element Should Contain    id=message    Gender does not meet the conditions